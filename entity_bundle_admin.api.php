<?php

/**
 * @file
 * Hooks provided by the Entity Bundle Admin module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide basic bundle admin UI.
 *
 * This is a placeholder for describing further keys for hook_entity_info(),
 * which are introduced by the entity bundle admin module for providing a UI for
 * an entity type's bundles.
 *
 *  -'bundle admin ui': An array containing the following properties:
 *    - 'path': The base path for the admin UI, eg 'admin/structure/TYPE'.
 *      Each bundle will get a path at PATH/manage/BUNDLE.
 *    - 'permission': The permission string to use for access to the admin UI
 *      pages.
 *    - 'controller class' (optional) The name of a class to use as a controller
 *      for the bundle admin UI generation. Defaults to
 *      EntityBundleAdminDefaultUIController.
 *    - 'bundle hook': (optional) The hook to invoke to collect bundle type
 *      definitions. Defaults to hook_entity_bundle_ENTITY_TYPE_bundle_info().
 *
 * @see hook_entity_info()
 */
function entity_operations_hook_entity_info() {
}

/**
 * Declare bundles for an entity.
 *
 * @return
 *  An array keyed by bundle machine name, whose values are arrays with the
 *  following properties:
 *  - 'label': The bundle label.
 *  - 'description': A description of the bundle, for use in the admin UI.
 *    This is added to the entity info, for modules such as Entity Operations to
 *    make use of.
 */
function hook_entity_bundle_ENTITY_TYPE_bundle_info() {
  return array(
    'mybundle' => array(
      'label' => t('My bundle'),
      'description' => t('Description text'),
    ),
  );
}
